# create_fill_database.py

import mysql.connector as mc

conn= mc.connect(host='localhost',user='root',password='d',db='pythonSQL')
c = conn.cursor()

def create_table():
    c.execute('DROP TABLE IF EXISTS writer')
    c.execute('CREATE TABLE writer \
            (                   \
              id    INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, \
              name  VARCHAR(30) NOT NULL UNIQUE,  \
              age   int \
            )' 
    )

def insert_data():

    c.execute("INSERT INTO writer (name) VALUES ('Pearl Buck')")

    c.execute(" INSERT INTO writer VALUES \
        (NULL, 'Rabindranath Tagore', 80), \
        (NULL, 'Leo Tolstoy', 82)" \
    )

    c.execute(" INSERT INTO writer (age, name) VALUES \
        (30, 'Meher Krishna Patel')" \
    )

def commit_close():
    """ commit changes to database and close connection """ 

    conn.commit()
    c.close()
    conn.close()



def main():
    create_table()
    insert_data()
    commit_close()

if __name__ == '__main__':
    main()
