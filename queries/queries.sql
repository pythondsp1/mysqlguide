DROP DATABASE IF EXISTS writerdb;
CREATE DATABASE writerdb;
DROP DATABASE writerdb;


DROP DATABASE IF EXISTS writerdb;
CREATE DATABASE writerdb;
USE writerdb;
SELECT DATABASE();    



DROP TABLE IF EXISTS writer;
CREATE TABLE writer
(
  id    INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name  VARCHAR(30) NOT NULL UNIQUE,
  age   INT
);



#method 1: Fill in correct sequnces
# Since 'id' is AUTO_INCREMENT, therefore NULL is set for this.
INSERT INTO writer VALUES
  (NULL, 'Rabindranath Tagore', 80),
  (NULL, 'Leo Tolstoy', 82);

#Method 2:  skip age as it it optional
INSERT INTO writer (name) VALUES ('Pearl Buck');

# Method 3: fill with keywords args i.e (age, name)
# Since 'id' is PRIMARY KEY AUTO_INCREMENT, 
# therefore it is not passed in INSERT statement.
INSERT INTO writer (age, name) VALUES
  (30, 'Meher Krishna Patel');

SELECT * FROM writer;
SELECT name FROM writer;


DROP TABLE IF EXISTS book;
    CREATE TABLE book # creating table 'book'
    (
     writer_id  INT UNSIGNED NOT NULL,               
     book_id  INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
     title VARCHAR(100) NOT NULL,                
     price INT UNSIGNED
    );

# Insert data in the table
INSERT INTO book (writer_id,title,price)
  SELECT id, 'The Good Earth', 200         # select id
  FROM writer WHERE name = 'Pearl Buck';   # where name = 'Pearl Buck'

INSERT INTO book (writer_id,title,price)
  SELECT id, 'The Home and The World',  250
  FROM writer WHERE name = 'Rabindranath Tagore';

INSERT INTO book (writer_id,title,price)
  SELECT id, 'Gitanjali', 100
  FROM writer WHERE name = 'Rabindranath Tagore';

INSERT INTO book (writer_id,title,price)
	SELECT id, 'War and Peace', 200
	FROM writer WHERE name = 'Leo Tolstoy';

INSERT INTO book (writer_id,title,price)
	SELECT id, 'Anna Karenina', 100
	FROM writer WHERE name = 'Leo Tolstoy';


#use database writerdb
USE writerdb;

#create table
DROP TABLE IF EXISTS student;
CREATE TABLE student
(
  id    INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name  VARCHAR(30) NOT NULL UNIQUE,
  gender nvarchar(5),
  age   int, 
  city  nvarchar(30),
  marks int
);

#insert data
INSERT INTO student (name, gender, age, city, marks) VALUES
  ('Tom', 'M', 20, 'NY', 30),
  ('Kim', 'F', 18, 'NY', 40),
  ('Sam', 'M', 18, 'NY', 60),
  ('Kris', 'M', 20, 'DEL', 80),
  ('Harry', 'M', 19, 'DEL', 70),
  ('Eliza', 'F', 19, 'DEL', 50),
  ('Kate', 'F', 15, 'ACK', 20),
  ('Peter', 'M', 21, 'ACK', 80),
  ('Ryan', 'M', 20, 'ACK', 60);
  
  
# writing age once is compulsory, as HAVING operation is performed on age.
# replace both ages from below query, and it will give blank result.

SELECT age, SUM(marks), COUNT(id) FROM student 
GROUP BY age HAVING age < 20;

#writing age is is not compulsory in WHERE.
SELECT SUM(marks), COUNT(id) FROM student 
WHERE age < 20 GROUP BY age;


# simple store procedure
# use database
USE writerdb;

# creating store procedure
DELIMITER //
CREATE PROCEDURE cpGetWriter()
 BEGIN
 SELECT * FROM writer;
 END //
DELIMITER ;

# calling store procedure
CALL cpGetWriter();

# results will be same as 
#SELECT * FROM writer;




#Store procedure with input
USE writerdb; #use database

#create store procedure
DELIMITER //
CREATE PROCEDURE cpBookDetails(IN writerID INT)
BEGIN
SELECT id, name, title, price FROM writer
INNER JOIN book 
ON writer.id  = book.writer_id WHERE id=writerID;
END //
DELIMITER ;


# VIEW Example 1:
USE writerdb;

#create VIEW: display title and price of books.
CREATE VIEW BookPrice AS 
  SELECT title, price FROM book;

#using VIEW as table
select * from BookPrice;




# ADD CONSTRAINT to existing table:
ALTER TABLE book
  ADD CONSTRAINT fk_Writer 
   FOREIGN KEY key_writer(writer_id)
   REFERENCES writer(id)
   ON UPDATE CASCADE
   ON DELETE CASCADE;
#CONSTRAINT will not add, if there is already some writer_id in 'book', which is not present in 'writer' table.
