.. _recordSelection:

Record selection
================

Various useful MySQL commands are shown in Chapter :ref:`mysql-overview`. Then, Chapter :ref:`pythonMySQL` shows the way in which MySQL queries can be used in Python. In this chapter, we will discuss some more record selection features of MySQL, which can be quite useful in processing the data efficiently. 


Using MySQL client
------------------

.. note:: Please see Section :ref:`connectionparameter`, before jumping to sections after that.


Connect and terminate database
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To connect with database, we need to provide the hostname, username and password as shown below, 

.. code-block:: shell

    $ mysql -h hostname -u username -p

* By default, hostname(-h) is set to 'localhost' and username (-u) is set to 'your login name'. To terminate the database connection, 'QUIT' command is used as shown in Chapter :ref:`mysql-overview`. 


Autocomplete
^^^^^^^^^^^^

Use 'tab' button to autocomplete the query. Note that autocomplete work for uppercase keywords i.e. if we press tab after writing 'SEL', it will complete it to 'SELECT' or show the possible options; whereas it will not work for lowercase keywords 'sel'. 

Display output of query on terminal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Following code can be used to display the output of a query on the terminal. Note that, query is run through the terminal, 

.. code-block:: shell

    $ echo 'SELECT * FROM writer' | mysql -h localhost -u root -p writerdb
    Enter password: 
    id	name	age
    1	Rabindranath Tagore	80
    2	Leo Tolstoy	82
    3	Pearl Buck	NULL
    4	Meher Krishna Patel	30


.. _connectionparameter:

Save connection parameter
^^^^^^^^^^^^^^^^^^^^^^^^^

In the above command, we need to specify the connection parameters along with the queries; which can be quite annoying for repetitive queries. The connection parameters can be saved to a file '.my.cnf' in the home directory as shown below, 

.. code-block:: shell
    
    $ cd
    $ cat >> .my.cnf
    # .my.cnf
    [client]
    host=localhost
    user=root
    password=xyz  # press ctrl-c to exit
    

* Now, command in previous section can be executed without connection parameters, as shown below, 

.. code-block:: shell
    
    $ echo 'SELECT * FROM writer' | mysql writerdb
    id	name	age
    1	Rabindranath Tagore	80
    2	Leo Tolstoy	82
    3	Pearl Buck	NULL
    4	Meher Krishna Patel	30


* Further, protect the '.my.cnf' file from others by changing it's permission as follows, 

.. code-block:: shell

    $ chmod 600 .my.cnf

Queries from command line
^^^^^^^^^^^^^^^^^^^^^^^^^

In previous two sections, the unix pipe ( | ) is used to feed the output of echo command to mysql. But there is a nicer way to execute the query using command prompt, i.e. using -e (execute) as shown below. Note that, name of the database is at the end of the query. 

.. code-block:: shell
  
    $ # note that multiple queries are executed in below code, 
    $ mysql -e  'SELECT * FROM writer; SELECT NOW()' writerdb
    +----+---------------------+------+
    | id | name                | age  |
    +----+---------------------+------+
    |  1 | Rabindranath Tagore |   80 |
    |  2 | Leo Tolstoy         |   82 |
    |  3 | Pearl Buck          | NULL |
    |  4 | Meher Krishna Patel |   30 |
    +----+---------------------+------+
    +---------------------+
    | NOW()               |
    +---------------------+
    | 2017-03-01 18:09:57 |
    +---------------------+

Terminating partial query
^^^^^^^^^^^^^^^^^^^^^^^^^

'\\c' is used to terminate the query i.e. when we do not want to execute the query, we can cancel it by '\\c', 

.. code-block:: mysql

    mysql> SELECT * FROM \c

SQL variables
^^^^^^^^^^^^^

We can assign variable names while executing the SELECT statements; and then those variables can be used with other statements as shown below, 

.. code-block:: mysql

    mysql> USE writerdb
    Database changed

    # defining variable 'n'
    mysql> SELECT @n := name FROM writer WHERE name = 'Leo Tolstoy';
    +-------------+
    | @n := name  |
    +-------------+
    | Leo Tolstoy |
    +-------------+
    1 row in set (0.00 sec)

    # using variable 'n'
    mysql> SELECT * FROM writer WHERE name = @n;
    +----+-------------+------+
    | id | name        | age  |
    +----+-------------+------+
    |  2 | Leo Tolstoy |   82 |
    +----+-------------+------+
    1 row in set (0.01 sec)

* SELECT can be used see the value of the variable, 

.. code-block:: mysql

    mysql> select @n;
    +-------------+
    | @n          |
    +-------------+
    | Leo Tolstoy |
    +-------------+
    1 row in set (0.00 sec)

* If a statement returns multiple values that last value will be stored in the variable; whereas it statement returns no value, then NULL will be stored. 

* To store a fix value to a variable, 'SET' command is used, 

.. code-block:: mysql

    mysql> SET @s = 3 + 9;
    Query OK, 0 rows affected (0.00 sec)

    mysql> SELECT @s;
    +------+
    | @s   |
    +------+
    |   12 |
    +------+
    1 row in set (0.00 sec)

.. note:: 

    In above code, SELECT statement is performing the mathematical operations (not the SET statement). Some more examples are shown below, 

   .. code-block:: shell

        mysql> SELECT (3+2)/2;
        +---------+
        | (3+2)/2 |
        +---------+
        |  2.5000 |
        +---------+
        1 row in set (0.00 sec)

        mysql> SELECT 1/0;
        +------+
        | 1/0  |
        +------+
        | NULL |
        +------+
        1 row in set, 1 warning (0.00 sec)
    
        mysql> SELECT 'MEHER' = 'KRISHNA';
        +---------------------+
        | 'MEHER' = 'KRISHNA' |
        +---------------------+
        |                   0 |
        +---------------------+
        1 row in set (0.00 sec)

        mysql> SELECT 'MEHER' = 'meher';
        +-------------------+
        | 'MEHER' = 'meher' |
        +-------------------+
        |                 1 |
        +-------------------+
        1 row in set (0.00 sec)




Reading query from file
^^^^^^^^^^^^^^^^^^^^^^^

 * Let's save a simple query in the 'writers.sql' file, as shown below, 

.. code-block:: shell 
    
    $ cat > writers.sql
    USE writerdb; 
    SELECT * FROM writer;
    

Then this query can be run from the terminal as shown below, 

.. code-block:: shell

    $ mysql < writers.sql 
    id	name	age
    1	Rabindranath Tagore	80
    2	Leo Tolstoy	82
    3	Pearl Buck	NULL
    4	Meher Krishna Patel	30

* Now, remove the first line from the writers.db file i.e USE writerdb, 

.. code-block:: shell

    $ cat > writers.sql 
    SELECT * FROM writer;

To execute this code, we need to provide the 'table name' explicitly, 

.. code-block:: shell
    
    $ mysql writerdb < writers.sql 
    id	name	age
    1	Rabindranath Tagore	80
    2	Leo Tolstoy	82
    3	Pearl Buck	NULL
    4	Meher Krishna Patel	30


* 'SOURCE' is used to execute query from 'mysql' prompt, 

.. code-block:: mysql

    mysql> USE writerdb;
    Database changed
    mysql> SOURCE writers.sql
    +----+---------------------+------+
    | id | name                | age  |
    +----+---------------------+------+
    |  1 | Rabindranath Tagore |   80 |
    |  2 | Leo Tolstoy         |   82 |
    |  3 | Pearl Buck          | NULL |
    |  4 | Meher Krishna Patel |   30 |
    +----+---------------------+------+
    4 rows in set (0.00 sec)


Saving output to file
^^^^^^^^^^^^^^^^^^^^^

We can send the output of the query to some file using unix commands as follows, 

.. code-block:: shell

    $ mysql -e  'SELECT * FROM writer; SELECT NOW()' writerdb  >  writers.txt

    $ cat writers.txt
    id	name	age
    1	Rabindranath Tagore	80
    2	Leo Tolstoy	82
    3	Pearl Buck	NULL
    4	Meher Krishna Patel	30
    NOW()
    2017-03-01 18:16:35

Saving output as CSV file
^^^^^^^^^^^^^^^^^^^^^^^^^

Unix 'sed' command along with 'regular expression' can be used to convert the output into csv format. 
In the below code, tab (\\t) is converted into comma (,) using 'sed' command,

.. code-block:: shell

    
    $ mysql -e  "SELECT * FROM writer" writerdb | sed -e "s/\t/,/g" > writers.txt 

    $ cat writers.txt 
    id,name,age
    1,Rabindranath Tagore,80
    2,Leo Tolstoy,82
    3,Pearl Buck,NULL
    4,Meher Krishna Patel,30

* Same can be achieved by reading the file as well as shown below, 

.. code-block:: shell

    $ mysql writerdb < writers.sql | sed -e "s/\t/,/g" > writers.txt
    
    $ cat writers.txt
    id,name,age
    1,Rabindranath Tagore,80
    2,Leo Tolstoy,82
    3,Pearl Buck,NULL
    4,Meher Krishna Patel,30

HTML and XML outputs
^^^^^^^^^^^^^^^^^^^^

HTML and XML outputs can be created using -H and -X options respectively, 

.. code-block:: shell

    $ # HTML  
    $ mysql -H -e  'SELECT * FROM writer; SELECT NOW()' writerdb

    $ # XML  
    $ mysql -X -e  'SELECT * FROM writer; SELECT NOW()' writerdb

Skipping Header from the output
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

'-N' can be used to skip the header row, as shown below, 

.. code-block:: shell

    
    $ mysql -N -e  "SELECT * FROM writer" writerdb
    +---+---------------------+------+
    | 1 | Rabindranath Tagore |   80 |
    | 2 |         Leo Tolstoy |   82 |
    | 3 |          Pearl Buck | NULL |
    | 4 | Meher Krishna Patel |   30 |
    +---+---------------------+------+

* Following unix commands also generate the same result as above, 

.. code-block:: shell

    
    $ mysql -e  "SELECT * FROM writer" writerdb | tail -n +2
    
    $ mysql -e  "SELECT * FROM writer" writerdb | tail --lines=+2

Displaying long results
^^^^^^^^^^^^^^^^^^^^^^^

To display the long results properly, '\\G' option can be used, 

.. code-block:: shell

    mysql> USE writerdb;
    Database changed
    mysql> SELECT * FROM writer\G;
    *************************** 1. row ***************************
      id: 1
    name: Rabindranath Tagore
     age: 80
    *************************** 2. row ***************************
      id: 2
    name: Leo Tolstoy
     age: 82
    *************************** 3. row ***************************
      id: 3
    name: Pearl Buck
     age: NULL
    *************************** 4. row ***************************
      id: 4
    name: Meher Krishna Patel
     age: 30
    4 rows in set (0.00 sec)

* To see complete details of columns, use following 'SHOW FULL COLUMNS' as below, 

 .. code-block:: shell
     
     mysql> SHOW FULL COLUMNS FROM writer\G;
     *************************** 1. row ***************************
          Field: id
           Type: int(10) unsigned
      Collation: NULL
           Null: NO
            Key: PRI
        Default: NULL
          Extra: auto_increment
     Privileges: select,insert,update,references
        Comment: 
     *************************** 2. row ***************************
          Field: name
           Type: varchar(30)
      Collation: latin1_swedish_ci
           Null: NO
            Key: UNI
        Default: NULL
          Extra: 
     Privileges: select,insert,update,references
        Comment: 
     *************************** 3. row ***************************
          Field: age
           Type: int(11)
      Collation: NULL
           Null: YES
            Key: 
        Default: NULL
          Extra: 
     Privileges: select,insert,update,references
        Comment: 
     3 rows in set (0.00 sec)

.. _moreonselect:

More on SELECT statement
------------------------

In this section, we will focus on SELECT statement retrieve information from the database. First create a new table using 'mail.sql' files whose contents are shown below, 

.. code-block:: mysql
    
    # mail.sql

    DROP TABLE IF EXISTS mail;
    #@ _CREATE_TABLE_
    CREATE TABLE mail
    (
      t       DATETIME,    # when message was sent
      srcuser VARCHAR(8),  # sender (source user and host)
      srchost VARCHAR(20),
      dstuser VARCHAR(8),  # recipient (destination user and host)
      dsthost VARCHAR(20),
      size    BIGINT,      # message size in bytes
      INDEX (t)
    );
    #@ _CREATE_TABLE_

    INSERT INTO mail (t,srchost,srcuser,dsthost,dstuser,size)
      VALUES
        ('2014-05-11 10:15:08','saturn','barb','mars','tricia',58274),
        ('2014-05-12 12:48:13','mars','tricia','venus','gene',194925),
        ('2014-05-12 15:02:49','mars','phil','saturn','phil',1048),
        ('2014-05-12 18:59:18','saturn','barb','venus','tricia',271),
        ('2014-05-14 09:31:37','venus','gene','mars','barb',2291),
        ('2014-05-14 11:52:17','mars','phil','saturn','tricia',5781),
        ('2014-05-14 14:42:21','venus','barb','venus','barb',98151),
        ('2014-05-14 17:03:01','saturn','tricia','venus','phil',2394482),
        ('2014-05-15 07:17:48','mars','gene','saturn','gene',3824),
        ('2014-05-15 08:50:57','venus','phil','venus','phil',978),
        ('2014-05-15 10:25:52','mars','gene','saturn','tricia',998532),
        ('2014-05-15 17:35:31','saturn','gene','mars','gene',3856),
        ('2014-05-16 09:00:28','venus','gene','mars','barb',613),
        ('2014-05-16 23:04:19','venus','phil','venus','barb',10294),
        ('2014-05-19 12:49:23','mars','phil','saturn','tricia',873),
        ('2014-05-19 22:21:51','saturn','gene','venus','gene',23992)
    ;

* Use following commands to create and load the content of 'mail.sql' file to database, 
  
.. code-block:: mysql

    
    $ mysql -u root -p
    Enter password: 

    
    # create and use database 'mysqldb'
    mysql> CREATE DATABASE mysqldb;
    mysql> USE mysqldb;
    Database changed
    
    # load data from 'mail.sql'
    mysql> SOURCE mail.sql
    Query OK, 0 rows affected, 1 warning (0.00 sec)

    Query OK, 0 rows affected (0.40 sec)

    Query OK, 16 rows affected (0.15 sec)
    Records: 16  Duplicates: 0  Warnings: 0

    # check data in the new table 'mail'
    mysql> SELECT * FROM mail;
    +---------------------+---------+---------+---------+---------+---------+
    | t                   | srcuser | srchost | dstuser | dsthost | size    |
    +---------------------+---------+---------+---------+---------+---------+
    | 2014-05-11 10:15:08 | barb    | saturn  | tricia  | mars    |   58274 |
    | 2014-05-12 12:48:13 | tricia  | mars    | gene    | venus   |  194925 |
    | 2014-05-12 15:02:49 | phil    | mars    | phil    | saturn  |    1048 |
    | 2014-05-12 18:59:18 | barb    | saturn  | tricia  | venus   |     271 |
    | 2014-05-14 09:31:37 | gene    | venus   | barb    | mars    |    2291 |
    | 2014-05-14 11:52:17 | phil    | mars    | tricia  | saturn  |    5781 |
    | 2014-05-14 14:42:21 | barb    | venus   | barb    | venus   |   98151 |
    | 2014-05-14 17:03:01 | tricia  | saturn  | phil    | venus   | 2394482 |
    | 2014-05-15 07:17:48 | gene    | mars    | gene    | saturn  |    3824 |
    | 2014-05-15 08:50:57 | phil    | venus   | phil    | venus   |     978 |
    | 2014-05-15 10:25:52 | gene    | mars    | tricia  | saturn  |  998532 |
    | 2014-05-15 17:35:31 | gene    | saturn  | gene    | mars    |    3856 |
    | 2014-05-16 09:00:28 | gene    | venus   | barb    | mars    |     613 |
    | 2014-05-16 23:04:19 | phil    | venus   | barb    | venus   |   10294 |
    | 2014-05-19 12:49:23 | phil    | mars    | tricia  | saturn  |     873 |
    | 2014-05-19 22:21:51 | gene    | saturn  | gene    | venus   |   23992 |
    +---------------------+---------+---------+---------+---------+---------+
    16 rows in set (0.00 sec)
  

Give name to columns
^^^^^^^^^^^^^^^^^^^^

Currently, the column names (i.e. srcuser, dstuser adn dsthost etc.) do not provide useful information. We can change these names while selecting the data as follows, 

.. code-block:: mysql
    
    mysql> SELECT
        -> srcuser AS 'Message Sender',
        -> srchost AS 'Source Host',
        -> dstuser AS 'Message Receiver',
        -> dsthost AS 'Destination Host'
        -> FROM mail;
    +----------------+-------------+------------------+------------------+
    | Message Sender | Source Host | Message Receiver | Destination Host |
    +----------------+-------------+------------------+------------------+
    | barb           | saturn      | tricia           | mars             |
    | tricia         | mars        | gene             | venus            |
    | [...]                                                              |
    | gene           | saturn      | gene             | venus            |
    +----------------+-------------+------------------+------------------+
    16 rows in set (0.00 sec)

Combining columns
^^^^^^^^^^^^^^^^^

Suppose, we want to combine the columns e.g. 'Source host' and 'Message Sender', then it can be done using 'CONCAT' keyword, as shown below. Note that, we can use any or no symbol in CONCAT e.g. '-' and '@' is used in below code, 

.. code-block:: mysql
    
    mysql> SELECT
        -> CONCAT(srcuser, '-', srchost) AS sender, # e.g. barb-saturn
        -> CONCAT(dstuser, '@', dsthost) AS receiver # e.g. gene@veus
        -> FROM mail;
    +---------------+---------------+
    | sender        | receiver      |
    +---------------+---------------+
    | barb-saturn   | tricia@mars   |
    | tricia-mars   | gene@venus    |
    | phil-mars     | phil@saturn   |
    | barb-saturn   | tricia@venus  |
    | [...]                         |
    | gene-saturn   | gene@venus    |
    +---------------+---------------+
    16 rows in set (0.00 sec)

WHERE (specify selection)
^^^^^^^^^^^^^^^^^^^^^^^^^

WHERE is used to select specific rows from the table. 

* Select rows with dsthost='venus' 

.. code-block:: mysql

    mysql> SELECT * FROM mail WHERE dsthost='venus';
    +---------------------+---------+---------+---------+---------+---------+
    | t                   | srcuser | srchost | dstuser | dsthost | size    |
    +---------------------+---------+---------+---------+---------+---------+
    | 2014-05-12 12:48:13 | tricia  | mars    | gene    | venus   |  194925 |
    | 2014-05-12 18:59:18 | barb    | saturn  | tricia  | venus   |     271 |
    | 2014-05-14 14:42:21 | barb    | venus   | barb    | venus   |   98151 |
    | [...]                                                                 |
    | 2014-05-19 22:21:51 | gene    | saturn  | gene    | venus   |   23992 |
    +---------------------+---------+---------+---------+---------+---------+
    7 rows in set (0.00 sec)


* Select rows where srchost starts with 'g',

.. code-block:: mysql

    
    mysql> SELECT * FROM mail WHERE srchost LIKE 's%';
    +---------------------+---------+---------+---------+---------+---------+
    | t                   | srcuser | srchost | dstuser | dsthost | size    |
    +---------------------+---------+---------+---------+---------+---------+
    | 2014-05-11 10:15:08 | barb    | saturn  | tricia  | mars    |   58274 |
    | 2014-05-12 18:59:18 | barb    | saturn  | tricia  | venus   |     271 |
    | 2014-05-14 17:03:01 | tricia  | saturn  | phil    | venus   | 2394482 |
    | 2014-05-15 17:35:31 | gene    | saturn  | gene    | mars    |    3856 |
    | 2014-05-19 22:21:51 | gene    | saturn  | gene    | venus   |   23992 |
    +---------------------+---------+---------+---------+---------+---------+
    5 rows in set (0.00 sec)

* Select rows with srcuser = 'barb' and dstuser = 'tricia', 

.. code-block:: mysql

    mysql> SELECT * FROM mail WHERE srcuser='barb' AND dstuser = 'tricia';
    +---------------------+---------+---------+---------+---------+-------+
    | t                   | srcuser | srchost | dstuser | dsthost | size  |
    +---------------------+---------+---------+---------+---------+-------+
    | 2014-05-11 10:15:08 | barb    | saturn  | tricia  | mars    | 58274 |
    | 2014-05-12 18:59:18 | barb    | saturn  | tricia  | venus   |   271 |
    +---------------------+---------+---------+---------+---------+-------+
    2 rows in set (0.00 sec)

* Select rows where srcuser=dstuser and srchost=dsthost,  

.. code-block:: mysql

    
    mysql> SELECT * FROM mail WHERE srcuser=dstuser AND srchost=dsthost;
    +---------------------+---------+---------+---------+---------+-------+
    | t                   | srcuser | srchost | dstuser | dsthost | size  |
    +---------------------+---------+---------+---------+---------+-------+
    | 2014-05-14 14:42:21 | barb    | venus   | barb    | venus   | 98151 |
    | 2014-05-15 08:50:57 | phil    | venus   | phil    | venus   |   978 |
    +---------------------+---------+---------+---------+---------+-------+
    2 rows in set (0.00 sec)

IN clause (for OR operation)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

IN clause can be used for performing the OR operation, 

.. code-block:: mysql

    mysql> SELECT * FROM mail WHERE srcuser IN ('barb', 'gene');
    +---------------------+---------+---------+---------+---------+--------+
    | t                   | srcuser | srchost | dstuser | dsthost | size   |
    +---------------------+---------+---------+---------+---------+--------+
    | 2014-05-11 10:15:08 | barb    | saturn  | tricia  | mars    |  58274 |
    | 2014-05-12 18:59:18 | barb    | saturn  | tricia  | venus   |    271 |
    | 2014-05-14 09:31:37 | gene    | venus   | barb    | mars    |   2291 |
    | [... ]                                                               |
    | 2014-05-19 22:21:51 | gene    | saturn  | gene    | venus   |  23992 |
    +---------------------+---------+---------+---------+---------+--------+
    9 rows in set (0.00 sec)


NULL values
^^^^^^^^^^^

Let's, add some more entries to table 'mail' as follows, 

.. code-block:: mysql

    mysql> INSERT INTO mail (t,srcuser,dstuser,size)
    ->   VALUES
    ->     ('2014-05-11 10:15:08','barb','tricia',58274),
    ->     ('2014-05-12 12:48:13','tricia','gene',194925),
    ->     ('2014-05-12 15:02:49','phil','saturn',1048),
    ->     ('2014-05-12 18:59:18','barb','tricia',271)
    -> ;
    Query OK, 4 rows affected (0.11 sec)
    Records: 4  Duplicates: 0  Warnings: 0

Search the NULL values
""""""""""""""""""""""

* In above insertion, 'srchost' and 'dsthost' columns are neglected, therefore these values will be filled as 'NULL'
* Now, we can see some NULL entries as follows. Note that, 'srchost=NULL' does not return the correct answers; hence for NULL values use 'srchost IS NULL' as shown below, 
* Similarly, use 'srchost IS NOT NULL (instead of srchost != NULL). 

.. code-block:: mysql

    
    mysql> SELECT * FROM mail where srchost=NULL;
    Empty set (0.00 sec)

    mysql> SELECT * FROM mail where srchost IS NULL;
    +---------------------+---------+---------+---------+---------+--------+
    | t                   | srcuser | srchost | dstuser | dsthost | size   |
    +---------------------+---------+---------+---------+---------+--------+
    | 2014-05-11 10:15:08 | barb    | NULL    | tricia  | NULL    |  58274 |
    | 2014-05-12 12:48:13 | tricia  | NULL    | gene    | NULL    | 194925 |
    | 2014-05-12 15:02:49 | phil    | NULL    | saturn  | NULL    |   1048 |
    | 2014-05-12 18:59:18 | barb    | NULL    | tricia  | NULL    |    271 |
    +---------------------+---------+---------+---------+---------+--------+
    4 rows in set (0.00 sec)


NULL <=> NULL
"""""""""""""

NULL <=> NULL compares the NULL values and return '1' if values are NULL; whereas NULL=NULL returns NULL if values are NULL, as shown below, 

.. code-block:: mysql

    mysql> SELECT NULL=NULL, NULL<=>NULL;
    +-----------+-------------+
    | NULL=NULL | NULL<=>NULL |
    +-----------+-------------+
    |      NULL |           1 |
    +-----------+-------------+

Filling NULL with other values
""""""""""""""""""""""""""""""


.. code-block:: mysql

    
    mysql> SELECT srcuser, IF(srchost IS NULL, 'Unknown', srchost) FROM mail;
    +---------+-----------------------------------------+
    | srcuser | IF(srchost IS NULL, 'Unknown', srchost) |
    +---------+-----------------------------------------+
    | barb    | saturn                                  |
    | [...]                                             |
    | barb    | Unknown                                 |
    | tricia  | Unknown                                 |
    | phil    | Unknown                                 |
    | barb    | Unknown                                 |
    +---------+-----------------------------------------+
    20 rows in set (0.00 sec)

* Above method will will with any type of column-values, as shown below, 

.. code-block:: mysql

    
    mysql> SELECT srcuser, IF(srchost='mars', 'MARS', srchost) FROM mail;
    +---------+-------------------------------------+
    | srcuser | IF(srchost='mars', 'MARS', srchost) |
    +---------+-------------------------------------+
    | barb    | saturn                              |
    | tricia  | MARS                                |
    | phil    | MARS                                |
    | [...]                                         |
    | barb    | NULL                                |
    +---------+-------------------------------------+
    20 rows in set (0.00 sec)

* 'IFNULL' keyword can be used to fill the NULL value with some other values, 

.. code-block:: mysql

    mysql> SELECT srcuser, IFNULL(srchost, 'Unknown') FROM mail;
    +---------+----------------------------+
    | srcuser | IFNULL(srchost, 'Unknown') |
    +---------+----------------------------+
    | barb    | saturn                     |
    | tricia  | mars                       |
    | [...]                                |
    | barb    | Unknown                    |
    | tricia  | Unknown                    |
    | phil    | Unknown                    |
    | barb    | Unknown                    |
    +---------+----------------------------+
    20 rows in set (0.00 sec)o

Return part of result with LIMIL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* We can see first few lines of results using LIMIT keyword as follow, 

.. code-block:: mysql

    mysql> SELECT * FROM mail LIMIT 3;
    +---------------------+---------+---------+---------+---------+--------+
    | t                   | srcuser | srchost | dstuser | dsthost | size   |
    +---------------------+---------+---------+---------+---------+--------+
    | 2014-05-11 10:15:08 | barb    | saturn  | tricia  | mars    |  58274 |
    | 2014-05-12 12:48:13 | tricia  | mars    | gene    | venus   | 194925 |
    | 2014-05-12 15:02:49 | phil    | mars    | phil    | saturn  |   1048 |
    +---------------------+---------+---------+---------+---------+--------+
    3 rows in set (0.00 sec)

* Skip first 2 rows and show next 3 rows (i.e. rows 3-5), 

.. code-block:: mysql
    
    mysql> SELECT * FROM mail LIMIT 2,3;
    +---------------------+---------+---------+---------+---------+------+
    | t                   | srcuser | srchost | dstuser | dsthost | size |
    +---------------------+---------+---------+---------+---------+------+
    | 2014-05-12 15:02:49 | phil    | mars    | phil    | saturn  | 1048 |
    | 2014-05-12 18:59:18 | barb    | saturn  | tricia  | venus   |  271 |
    | 2014-05-14 09:31:37 | gene    | venus   | barb    | mars    | 2291 |
    +---------------------+---------+---------+---------+---------+------+
    3 rows in set (0.00 sec)
