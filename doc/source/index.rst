.. MySQL Guide documentation master file, created by
   sphinx-quickstart on Mon Feb 27 18:39:24 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MySQL Guide
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   mysql/index
   mysql/python
   mysql/recordSelection
   mysql/datetime



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
